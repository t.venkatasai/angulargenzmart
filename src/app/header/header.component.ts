import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  status:boolean=true;
  constructor() { }

  ngOnInit() {
    
  }
  ngDoCheck(){
    if(localStorage.getItem("token")){
        this.status=false;
    }
  }

    changeStatus(){
      if(localStorage.getItem("token")){
        localStorage.removeItem("token");
        this.status=true;
    }
    }

}

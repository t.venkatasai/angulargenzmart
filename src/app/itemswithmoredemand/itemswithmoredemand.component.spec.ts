import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemswithmoredemandComponent } from './itemswithmoredemand.component';

describe('ItemswithmoredemandComponent', () => {
  let component: ItemswithmoredemandComponent;
  let fixture: ComponentFixture<ItemswithmoredemandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemswithmoredemandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemswithmoredemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

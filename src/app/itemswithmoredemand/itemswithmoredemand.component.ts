import { Component, OnInit } from '@angular/core';
import { HttprequestserviceService } from '../httprequestservice.service';

@Component({
  selector: 'app-itemswithmoredemand',
  templateUrl: './itemswithmoredemand.component.html',
  styleUrls: ['./itemswithmoredemand.component.css']
})
export class ItemswithmoredemandComponent implements OnInit {

  itemInMoreDemand:any[]=[];
  constructor(private http:HttprequestserviceService) { }

  ngOnInit() {
    this.http.getItemsInMoreDemand().subscribe(data=>{
      this.itemInMoreDemand=data;
    })
  }
}

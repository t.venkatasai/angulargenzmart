import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandwisepredictionComponent } from './brandwiseprediction.component';

describe('BrandwisepredictionComponent', () => {
  let component: BrandwisepredictionComponent;
  let fixture: ComponentFixture<BrandwisepredictionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandwisepredictionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandwisepredictionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

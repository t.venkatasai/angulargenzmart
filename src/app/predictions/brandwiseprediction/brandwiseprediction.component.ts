import { Component, OnInit } from '@angular/core';
import { PdashboardService } from '../pdashboard.service';

@Component({
  selector: 'app-brandwiseprediction',
  templateUrl: './brandwiseprediction.component.html',
  styleUrls: ['./brandwiseprediction.component.css']
})
export class BrandwisepredictionComponent implements OnInit {
  brand:any;
  constructor(private pdash:PdashboardService) { }
  value:any;
  status:boolean=false;
  ngOnInit() {
  }

  
  getDetails(data){
    console.log(data);
    this.pdash.getBrandWiseSales(data).subscribe(data=>{
      this.value=data;
      this.status=true;
    });
  }
}

import { TestBed } from '@angular/core/testing';

import { PdashboardService } from './pdashboard.service';

describe('PdashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PdashboardService = TestBed.get(PdashboardService);
    expect(service).toBeTruthy();
  });
});

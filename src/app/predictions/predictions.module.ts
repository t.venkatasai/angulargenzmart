import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PredictionsRoutingModule } from './predictions-routing.module';
import { OverallsalesComponent } from './overallsales/overallsales.component';
import { FormsModule } from '@angular/forms';
import { PredictionsComponent } from './predictions.component';
import { CitypredictionsComponent } from './citypredictions/citypredictions.component';
import { CategorywisepredictionComponent } from './categorywiseprediction/categorywiseprediction.component';
import { BrandwisepredictionComponent } from './brandwiseprediction/brandwiseprediction.component';


@NgModule({
  declarations: [PredictionsComponent, OverallsalesComponent, CitypredictionsComponent, CategorywisepredictionComponent, BrandwisepredictionComponent],
  imports: [
    CommonModule,
    PredictionsRoutingModule,
    FormsModule
  ]
})
export class PredictionsModule { }

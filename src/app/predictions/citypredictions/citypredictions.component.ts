import { Component, OnInit } from '@angular/core';
import { PdashboardService } from '../pdashboard.service';

@Component({
  selector: 'app-citypredictions',
  templateUrl: './citypredictions.component.html',
  styleUrls: ['./citypredictions.component.css']
})
export class CitypredictionsComponent implements OnInit {
  months:any;
  constructor(private pdash:PdashboardService) { }
  value:any;
  status:boolean=false;
  ngOnInit() {
  }

  
  getDetails(data){
    console.log(data);
    this.pdash.getCityWiseSales(data).subscribe(data=>{
      this.value=data;
      this.status=true;
    });
  }

}

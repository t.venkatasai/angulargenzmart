import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitypredictionsComponent } from './citypredictions.component';

describe('CitypredictionsComponent', () => {
  let component: CitypredictionsComponent;
  let fixture: ComponentFixture<CitypredictionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitypredictionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitypredictionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

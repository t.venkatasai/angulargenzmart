import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PdashboardService {

  constructor(private http :HttpClient) { }

  getOverAllSales(data):Observable<any>{
    return this.http.get(`http://localhost:8080/SalesPrediction/${data.month}`);
  }

  getCityWiseSales(data):Observable<any>{
    return this.http.get(`http://localhost:8080/citySalesPrediction/${data.city_id}`);
  }

  getBrandWiseSales(data):Observable<any>{
    return this.http.get(`http://localhost:8080/brandSalesPrediction/${data.brand}`);
  }
  getCategoryWiseSales(data):Observable<any>{
    return this.http.get(`http://localhost:8080/categorySalesPrediction/${data.category}`);
  }
}

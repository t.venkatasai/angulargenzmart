import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverallsalesComponent } from './overallsales/overallsales.component';
import { PredictionsComponent } from './predictions.component';
import { CitypredictionsComponent } from './citypredictions/citypredictions.component';
import { CategorywisepredictionComponent } from './categorywiseprediction/categorywiseprediction.component';
import { BrandwisepredictionComponent } from './brandwiseprediction/brandwiseprediction.component';


const routes: Routes = [
  {
    path:'predictions',
    component:PredictionsComponent,
    children:[
      {
        path:'overallsales',
        component:OverallsalesComponent
      },
      {
        path:'citywisesales',
        component:CitypredictionsComponent
      },
      {
        path:'categorywise',
        component:CategorywisepredictionComponent
      },
      {
        path:'brandwise',
        component:BrandwisepredictionComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PredictionsRoutingModule { }

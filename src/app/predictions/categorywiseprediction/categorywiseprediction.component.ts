import { Component, OnInit } from '@angular/core';
import { PdashboardService } from '../pdashboard.service';

@Component({
  selector: 'app-categorywiseprediction',
  templateUrl: './categorywiseprediction.component.html',
  styleUrls: ['./categorywiseprediction.component.css']
})
export class CategorywisepredictionComponent implements OnInit {
  category:any;
  constructor(private pdash:PdashboardService) { }
  value:any;
  status:boolean=false;
  ngOnInit() {
  }

  
  getDetails(data){
    console.log(data);
    this.pdash.getCategoryWiseSales(data).subscribe(data=>{
      this.value=data;
      this.status=true;
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorywisepredictionComponent } from './categorywiseprediction.component';

describe('CategorywisepredictionComponent', () => {
  let component: CategorywisepredictionComponent;
  let fixture: ComponentFixture<CategorywisepredictionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorywisepredictionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorywisepredictionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { PdashboardService } from '../pdashboard.service';

@Component({
  selector: 'app-overallsales',
  templateUrl: './overallsales.component.html',
  styleUrls: ['./overallsales.component.css']
})
export class OverallsalesComponent implements OnInit {
  months:String[]=['201901']
  constructor(private pdash:PdashboardService) { }
  value:any;
  status:boolean=false;
  ngOnInit() {
  }

  
  getDetails(data){
    console.log(data);
    this.pdash.getOverAllSales(data).subscribe(data=>{
      this.value=data;
      this.status=true;
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindwhaturlookingComponent } from './findwhaturlooking.component';

describe('FindwhaturlookingComponent', () => {
  let component: FindwhaturlookingComponent;
  let fixture: ComponentFixture<FindwhaturlookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindwhaturlookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindwhaturlookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { HttprequestserviceService } from './httprequestservice.service';

describe('HttprequestserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttprequestserviceService = TestBed.get(HttprequestserviceService);
    expect(service).toBeTruthy();
  });
});

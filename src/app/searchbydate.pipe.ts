import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchbydate'
})
export class SearchbydatePipe implements PipeTransform {

 
  transform(orders: any[], searchWord: string): any {
    if (!searchWord)
    {
      return orders;
    }
    else
    {
      return orders.filter(searchedElement=>
        searchedElement.ddate.toLowerCase().indexOf(searchWord.toLowerCase())!=-1||
        searchedElement.item_name.toLowerCase().indexOf(searchWord.toLowerCase())!=-1||
        searchedElement.del_date.toString().indexOf(searchWord.toLowerCase())!=-1
      );
    }
  }

}

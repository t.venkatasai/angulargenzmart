import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemswithlessdemandComponent } from './itemswithlessdemand.component';

describe('ItemswithlessdemandComponent', () => {
  let component: ItemswithlessdemandComponent;
  let fixture: ComponentFixture<ItemswithlessdemandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemswithlessdemandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemswithlessdemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

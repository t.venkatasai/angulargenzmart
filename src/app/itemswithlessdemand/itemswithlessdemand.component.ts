import { Component, OnInit } from '@angular/core';
import { HttprequestserviceService } from '../httprequestservice.service';

@Component({
  selector: 'app-itemswithlessdemand',
  templateUrl: './itemswithlessdemand.component.html',
  styleUrls: ['./itemswithlessdemand.component.css']
})
export class ItemswithlessdemandComponent implements OnInit {

  itemInLessDemand:any[]=[];
  constructor(private http:HttprequestserviceService) { }

  ngOnInit() {
    this.http.getItemsInLessDemand().subscribe(data=>{
      this.itemInLessDemand=data;
    })
  }

}

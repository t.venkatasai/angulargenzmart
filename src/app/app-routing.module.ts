import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MaxordersComponent } from './maxorders/maxorders.component';
import { MinordersComponent } from './minorders/minorders.component';
import { StatusComponent } from './status/status.component';
import { FindwhaturlookingComponent } from './findwhaturlooking/findwhaturlooking.component';
import { LoginComponent } from './login/login.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';
import { ItemswithlessdemandComponent } from './itemswithlessdemand/itemswithlessdemand.component';
import { ItemswithprofitsComponent } from './dashboard/profit/itemswithprofits/itemswithprofits.component';
import { ItemswithmoredemandComponent } from './itemswithmoredemand/itemswithmoredemand.component';


const routes: Routes = [
                        {
                          path:'',
                          redirectTo:'home',
                          pathMatch:'full'
                        },
                        {path:'home',
                         component : HomeComponent,
                        children:[
                        {
                          path:'find',
                          component:FindwhaturlookingComponent,
                          children:[
                            {path:'maxorders',
                            component:MaxordersComponent},
                            {
                              path:'minorders',
                              component:MinordersComponent
                            },
                            {
                              path:'itemswithlessdemand',
                              component:ItemswithlessdemandComponent
                            },
                            {
                              path:'itemswithmoredemand',
                              component:ItemswithmoredemandComponent
                            },
                          
                          ]
                        }
                        ]},
                        {
                          path:'status',
                          component:StatusComponent
                        },
                        {
                          path:'login',
                          component:LoginComponent
                        },
                        {
                          path:'aboutus',
                          component:AboutusComponent
                        },
                        {
                          path:'contact',
                          component:ContactComponent
                        }
                      
                      
                         
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

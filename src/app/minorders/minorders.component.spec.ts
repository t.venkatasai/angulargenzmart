import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinordersComponent } from './minorders.component';

describe('MinordersComponent', () => {
  let component: MinordersComponent;
  let fixture: ComponentFixture<MinordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

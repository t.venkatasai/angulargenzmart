import { Component, OnInit } from '@angular/core';
import { HttprequestserviceService } from '../httprequestservice.service';

@Component({
  selector: 'app-minorders',
  templateUrl: './minorders.component.html',
  styleUrls: ['./minorders.component.css']
})
export class MinordersComponent implements OnInit {
  citiesWithMaxOrders:any[]=[];
  constructor(private httpService: HttprequestserviceService) { 
  }

  ngOnInit(){
   this.httpService.getnoOfCitiesWithMinCountOfOrders().subscribe(data=>this.citiesWithMaxOrders=data);
  }
}

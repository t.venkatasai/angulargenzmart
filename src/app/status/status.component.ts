import { Component, OnInit } from '@angular/core';
import { HttprequestserviceService } from '../httprequestservice.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {
  searchWord:string;
  allOrders:any[]=[];
  constructor(private httpService: HttprequestserviceService) { 
  }

  ngOnInit(){
   this.httpService.getAllOrdersList().subscribe(data=>this.allOrders=data);
  }

  getStatus(data){
    this.httpService.getParticularStatus(data).subscribe(data=>{
      this.allOrders=data;
    })
  }
}

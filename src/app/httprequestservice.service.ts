import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class HttprequestserviceService {

  constructor(private http:HttpClient) { 

  }


  getnoOfCitiesWithMaxCountOfOrders():Observable<any[]>
{
  return this.http.get<any[]>('http://localhost:8080/api/citiesWithMoreOrders');
}
getnoOfCitiesWithMinCountOfOrders():Observable<any[]>
{
  return this.http.get<any[]>('http://localhost:8080/api/citiesWithLessOrders');
}
getAllOrdersList():Observable<any[]>{
  return this.http.get<any[]>('http://localhost:8080/status');
}
sendLoginDetails(loginData):Observable<any>{
  return this.http.post<any>('http://localhost:8080/login',loginData,httpOptions);
}

getParticularStatus(data){
  var params = new HttpParams();
  params = params.append('month',data.month);
  params = params.append('year', data.year);
  return this.http.get<any>("http://localhost:8080/search/",{params: params});
}
getItemsInLessDemand():Observable<any[]>{
  return this.http.get<any[]>('http://localhost:8080/itemsWithLessOrders');
}
getItemsInMoreDemand():Observable<any[]>{
  return this.http.get<any[]>('http://localhost:8080/itemsWithMoreOrders');
}
}

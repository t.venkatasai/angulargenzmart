import { Component, OnInit } from '@angular/core';
import { HttprequestserviceService } from '../httprequestservice.service';

@Component({
  selector: 'app-maxorders',
  templateUrl: './maxorders.component.html',
  styleUrls: ['./maxorders.component.css']
})
export class MaxordersComponent implements OnInit {

  citiesWithMaxOrders: any[] = [];
  constructor(private httpService: HttprequestserviceService) {
  }

  ngOnInit() {
    this.httpService.getnoOfCitiesWithMaxCountOfOrders().subscribe(data => this.citiesWithMaxOrders = data);
  }

}

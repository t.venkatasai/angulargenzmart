import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaxordersComponent } from './maxorders.component';

describe('MaxordersComponent', () => {
  let component: MaxordersComponent;
  let fixture: ComponentFixture<MaxordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaxordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaxordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MinordersComponent } from './minorders/minorders.component';
import { MaxordersComponent } from './maxorders/maxorders.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { StatusComponent } from './status/status.component';
import { FindwhaturlookingComponent } from './findwhaturlooking/findwhaturlooking.component';
import { LoginComponent } from './login/login.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import {FormsModule}  from '@angular/forms';
import { DashboardModule } from './dashboard/dashboard.module';
import { ContactComponent } from './contact/contact.component';
import { ItemswithmoredemandComponent } from './itemswithmoredemand/itemswithmoredemand.component';
import { ItemswithlessdemandComponent } from './itemswithlessdemand/itemswithlessdemand.component';
import { SearchbydatePipe } from './searchbydate.pipe';
import { PredictionsModule } from './predictions/predictions.module';
@NgModule({
  declarations: [
    AppComponent,
    MinordersComponent,
    MaxordersComponent,
    HeaderComponent,
    HomeComponent,
    StatusComponent,
    FindwhaturlookingComponent,
    LoginComponent,
    AboutusComponent,
    ContactComponent,
    ItemswithmoredemandComponent,
    ItemswithlessdemandComponent,
    SearchbydatePipe,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DashboardModule,
    PredictionsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { CeoService } from '../../ceo.service';

@Component({
  selector: 'app-itemswithprofits',
  templateUrl: './itemswithprofits.component.html',
  styleUrls: ['./itemswithprofits.component.css']
})
export class ItemswithprofitsComponent implements OnInit {

  itemsWithProfits:any[]=[];
  constructor(private ceo:CeoService) { }

  ngOnInit() {
    this.ceo.getItemsProfit().subscribe(data=>{
      this.itemsWithProfits=data
    })
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemswithprofitsComponent } from './itemswithprofits.component';

describe('ItemswithprofitsComponent', () => {
  let component: ItemswithprofitsComponent;
  let fixture: ComponentFixture<ItemswithprofitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemswithprofitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemswithprofitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

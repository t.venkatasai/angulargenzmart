import { Component, OnInit } from '@angular/core';
import { CeoService } from '../../ceo.service';

@Component({
  selector: 'app-citieswithprofits',
  templateUrl: './citieswithprofits.component.html',
  styleUrls: ['./citieswithprofits.component.css']
})
export class CitieswithprofitsComponent implements OnInit {

  citiesWithProfits:any[]=[];
  constructor(private ceo:CeoService) { }

  ngOnInit() {
    this.ceo.getCitiesProfit().subscribe(data=>{
      this.citiesWithProfits=data
    })
  }

}

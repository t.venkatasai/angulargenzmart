import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitieswithprofitsComponent } from './citieswithprofits.component';

describe('CitieswithprofitsComponent', () => {
  let component: CitieswithprofitsComponent;
  let fixture: ComponentFixture<CitieswithprofitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitieswithprofitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitieswithprofitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { CeoService } from '../ceo.service';

@Component({
  selector: 'app-growth',
  templateUrl: './growth.component.html',
  styleUrls: ['./growth.component.css']
})
export class GrowthComponent implements OnInit {

  growthDetails:any[]=[];
  constructor(private ceo:CeoService) { }

  ngOnInit() {
    this.ceo.getGrowthDetails().subscribe(data=>{
        this.growthDetails=data;
        console.log(data);
    })
  }

}

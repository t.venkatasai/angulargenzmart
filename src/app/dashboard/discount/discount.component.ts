import { Component, OnInit } from '@angular/core';
import { CeoService } from '../ceo.service';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})
export class DiscountComponent implements OnInit {

  discountDetails:any[]=[];
  item:any;
  status:boolean=false;
  constructor(private ceo:CeoService) { }

  ngOnInit() {
  }
  getItemId(data){
   console.log(data);
   this.item=data;
   this.ceo.getItemDiscountDetails(data).subscribe(data=>{
  this.discountDetails=data;
  console.log(data);
  this.status=true;
   })
  }
}

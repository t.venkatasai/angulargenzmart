import { TestBed } from '@angular/core/testing';

import { CeoService } from './ceo.service';

describe('CeoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CeoService = TestBed.get(CeoService);
    expect(service).toBeTruthy();
  });
});

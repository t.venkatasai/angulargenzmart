import { Component, OnInit } from '@angular/core';
import { CeoService } from '../ceo.service';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {

  qtrYears:String[]=['Q1-2017','Q2-2017','Q3-2017','Q4-2017','Q1-2018','Q2-2018','Q3-2018','Q4-2018',
  'Q1-2019','Q2-2019','Q3-2019','Q4-2019','Q1-2020','Q2-2020','Q3-2020','Q4-2020']
  constructor(private ceo:CeoService) { }
  trendingItems:any[]=[];
  status:boolean=false;
  item:any;
  ngOnInit() {
  }

  getDetails(data){
    console.log(data);
    this.item=data;
    this.ceo.getItemInTrend(data).subscribe(data=>{
      this.trendingItems=data;
      console.log(data);
      this.status=true;
    })

  }
}

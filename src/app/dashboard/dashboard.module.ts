import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ProfitComponent } from './profit/profit.component';
import { DiscountComponent } from './discount/discount.component';
import { CitieswithprofitsComponent } from './profit/citieswithprofits/citieswithprofits.component';
import { ItemswithprofitsComponent } from './profit/itemswithprofits/itemswithprofits.component';
import { NetsalesComponent } from './netsales/netsales.component';
import { FormsModule } from '@angular/forms';
import { TrendsComponent } from './trends/trends.component';
import { GrowthComponent } from './growth/growth.component';
import { AuthorizationService } from './authorization.service';


@NgModule({
  declarations: [DashboardComponent, ProfitComponent, CitieswithprofitsComponent, ItemswithprofitsComponent, DiscountComponent, NetsalesComponent, TrendsComponent, GrowthComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers:[
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AuthorizationService,
      multi:true
    }
  ]
})
export class DashboardModule { }

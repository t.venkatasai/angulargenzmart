import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CeoService {

  constructor(private http:HttpClient) { }
  getCitiesProfit():Observable<any[]>{
    return this.http.get<any[]>("http://localhost:8080/citiesWithProfits");
  }

  getItemsProfit():Observable<any[]>{
    return this.http.get<any[]>("http://localhost:8080/itemsWithProfits");
  }

  getItemDiscountDetails(item):Observable<any[]>{
    return this.http.get<any[]>(`http://localhost:8080/discounts/${item.item_id}`);
  }

  getNetSales():Observable<any[]>{
    return this.http.get<any[]>("http://localhost:8080/netSalesItem");
  }

  getItemInTrend(data):Observable<any[]>{
    return this.http.get<any[]>(`http://localhost:8080/trends/itemsInTrending/${data.cityName}/${data.qtrYearSort}`);
  }
  getGrowthDetails():Observable<any[]>{
    return this.http.get<any[]>("http://localhost:8080/gettingGrowth");
  }
}

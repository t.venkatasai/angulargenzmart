import { Component, OnInit } from '@angular/core';
import { CeoService } from '../ceo.service';

@Component({
  selector: 'app-netsales',
  templateUrl: './netsales.component.html',
  styleUrls: ['./netsales.component.css']
})
export class NetsalesComponent implements OnInit {

  netSales:any[]=[];
  constructor(private ceo:CeoService) { }

  ngOnInit() {
    this.ceo.getNetSales().subscribe(data=>{
      this.netSales=data;
      console.log(data);
    })
  }

}

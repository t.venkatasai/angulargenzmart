import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetsalesComponent } from './netsales.component';

describe('NetsalesComponent', () => {
  let component: NetsalesComponent;
  let fixture: ComponentFixture<NetsalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetsalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetsalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

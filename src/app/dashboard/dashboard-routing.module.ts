import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ProfitComponent } from './profit/profit.component';
import { DiscountComponent } from './discount/discount.component';
import { CitieswithprofitsComponent } from './profit/citieswithprofits/citieswithprofits.component';
import { ItemswithprofitsComponent } from './profit/itemswithprofits/itemswithprofits.component';
import { NetsalesComponent } from './netsales/netsales.component';
import { TrendsComponent } from './trends/trends.component';
import { GrowthComponent } from './growth/growth.component';


const routes: Routes = [
                        {
                          path:'dashboard',
                          component:DashboardComponent,
                          children:[
                            {
                              path:'profit',
                              component:ProfitComponent,
                              children:[
                                {
                                  path:'citiesWithProfits',
                                  component:CitieswithprofitsComponent
                                },
                                {
                                  path:'itemsWithProfits',
                                  component:ItemswithprofitsComponent
                                }
                              ]
                            },
                            {
                              path:'discount',
                              component:DiscountComponent
                            },
                            {
                              path:'netsales',
                              component:NetsalesComponent
                            },
                            {
                              path:'trends',
                              component:TrendsComponent
                            },
                            {
                              path:'growth',
                              component:GrowthComponent
                            }
                          ]
                        },
                      
                       ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

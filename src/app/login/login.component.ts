import { Component, OnInit } from '@angular/core';
import { HttprequestserviceService } from '../httprequestservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  token:any;
  constructor(private http : HttprequestserviceService,private router:Router) { }

  ngOnInit() {
  }

  receiveData(loginData){
    
      this.http.sendLoginDetails(loginData).subscribe(data=>{
        this.token=data
        console.log(this.token);
        
        if ( data['token'] != undefined){
          localStorage.setItem("token", data['token']);
          this.router.navigate(['dashboard']);
        }
      });
      console.log(loginData);
      
  }
}
